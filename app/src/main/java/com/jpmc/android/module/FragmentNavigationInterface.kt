package com.jpmc.android.module


interface FragmentNavigationInterface {
    var navigationListener: NavigationListener?

    fun setNavigationCallbackListener(listener: NavigationListener)
}