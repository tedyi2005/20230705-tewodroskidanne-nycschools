package com.jpmc.android.module

import androidx.appcompat.widget.Toolbar

/**
 * Navigation listener interface for handling actions on fragments , should be implemented in MainActivity
 */
interface NavigationListener {
    fun onNavigationBackPressed()
    fun setUpToolbar(toolbar: Toolbar)
}