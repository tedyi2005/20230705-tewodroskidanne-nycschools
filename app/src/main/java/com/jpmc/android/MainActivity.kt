package com.jpmc.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.jpmc.android.databinding.ActivityMainBinding
import com.jpmc.android.fragments.HomeFragment
import com.jpmc.android.module.NavigationListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class MainActivity : AppCompatActivity(), NavigationListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var mainNavController: NavController

    // get current navigation fragment
    private val currentMainNavigationFragment: Fragment?
        get() = supportFragmentManager.findFragmentById(R.id.main_nav_host_fragment)
            ?.childFragmentManager
            ?.fragments
            ?.first()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navHostFragment: NavHostFragment =
            supportFragmentManager.findFragmentById(R.id.main_nav_host_fragment) as NavHostFragment
        mainNavController = navHostFragment.findNavController()
    }

    // Handle back press
    override fun onNavigationBackPressed() {
        binding.apply {
            if (currentMainNavigationFragment is HomeFragment) {
                finish()
            } else {
                onBackPressedDispatcher.onBackPressed()
            }
        }
    }

    override fun setUpToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)

        // Listen to destination changes on the Main Navigation
        mainNavController.addOnDestinationChangedListener { _, destination: NavDestination, _ ->

            when (destination.id) {
                R.id.home_fragment -> {
                    title = "Home"
                    NavigationUI.setupActionBarWithNavController(
                        this@MainActivity, mainNavController
                    )
                }

                R.id.fragment_details -> {
                    title = ""
                    NavigationUI.setupActionBarWithNavController(
                        this@MainActivity, mainNavController
                    )
                }
            }
        }

        // Override navigation OnClickListener so we can call onBackPressed and its callback
        // (defined in fragments' onCreate method, only for fragments which use it)
        toolbar.setNavigationOnClickListener {
            onBackPressedDispatcher.onBackPressed()
        }
    }
}