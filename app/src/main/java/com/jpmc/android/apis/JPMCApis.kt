package com.jpmc.android.apis

import com.jpmc.android.BuildConfig
import com.jpmc.android.datas.SchoolResult
import com.jpmc.android.datas.SchoolResultItem
import com.jpmc.android.datas.Schools
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *  APIs : BASE URL : https://data.cityofnewyork.us/resource/
 */
interface JPMCApis {
    @GET(BuildConfig.DOE_High_School_Directory_KEY + ".json")
    suspend fun getSchools(): Schools

    @GET(BuildConfig.SAT_RESULT_KEY + ".json")
    suspend fun getSATResult(
        @Query("dbn") dbn: String
    ): SchoolResult
}