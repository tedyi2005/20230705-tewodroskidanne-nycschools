package com.jpmc.android.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.gson.Gson
import com.jpmc.android.apis.UIResult
import com.jpmc.android.databinding.FragmentDetailsBinding
import com.jpmc.android.datas.SchoolResultItem
import com.jpmc.android.datas.SchoolsItem
import com.jpmc.android.utils.hide
import com.jpmc.android.utils.setTextOrHide
import com.jpmc.android.utils.show
import com.jpmc.android.viewmodels.JPMCViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

/**
 * Details Fragment class.
 */

@AndroidEntryPoint
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class DetailsFragment : BaseFragment() {

    private var _binding: FragmentDetailsBinding? = null
    private val binding get() = _binding!!
    private val jPMCViewModel: JPMCViewModel by viewModels()

    companion object {
        const val SCHOOL = "school"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailsBinding.inflate(inflater, container, false)
        setUpToolbarFromFragment(binding.toolbar.toolbar)
        val schoolsItem = arguments?.getString(SCHOOL)
        hide(binding.container)
        if (schoolsItem != null) {
            show(binding.spinner)
            jPMCViewModel.getSATResult(Gson().fromJson(schoolsItem, SchoolsItem::class.java).dbn)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            lifecycleScope.launch {
                jPMCViewModel.satResult.collect { result ->
                    when (result) {
                        is UIResult.Success -> {
                            val response = result.data
                            if (response.isNotEmpty())
                                setViews(response[0])
                            else
                                setUpErrorScreenBottomSheet("No SAT result data found", "Go Back") {
                                    requireActivity().onBackPressedDispatcher.onBackPressed()
                                }
                        }

                        is UIResult.Loading -> {
                            if (result.loading)
                                show(spinner)
                            else
                                hide(spinner)
                        }

                        is UIResult.Error -> {
                            hide(spinner)
                            setUpErrorScreenBottomSheet(result.error)
                        }
                    }
                }
            }
        }
    }

    private fun setViews(result: SchoolResultItem) {
        binding.apply {
            result.apply {
                show(container)
                schoolTitle.setTextOrHide(school_name)
                numOfSatTestTakersValue.setTextOrHide(num_of_sat_test_takers)
                satMathAvgScoreValue.setTextOrHide(sat_math_avg_score)
                satWritingAvgScoreValue.setTextOrHide(sat_writing_avg_score)
                satCriticalReadingAvgScoreValue.setTextOrHide(sat_critical_reading_avg_score)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}