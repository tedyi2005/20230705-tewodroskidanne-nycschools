package com.jpmc.android.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.jpmc.android.R
import com.jpmc.android.adapters.SchoolAdapter
import com.jpmc.android.apis.UIResult
import com.jpmc.android.databinding.FragmentHomeBinding
import com.jpmc.android.datas.SchoolsItem
import com.jpmc.android.utils.hide
import com.jpmc.android.utils.show
import com.jpmc.android.viewmodels.JPMCViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

/**
 * Home Fragment class.
 */

@AndroidEntryPoint
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class HomeFragment : BaseFragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val jPMCViewModel: JPMCViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        setUpToolbarFromFragment(binding.toolbar.toolbar)
        show(binding.spinner)
        jPMCViewModel.getSchools()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            lifecycleScope.launch {
                jPMCViewModel.schools.collect { result ->
                    when (result) {
                        is UIResult.Success -> {
                            initRecyclerViewWithSelection(result.data)
                        }

                        is UIResult.Loading -> {
                            if (result.loading)
                                show(spinner)
                            else
                                hide(spinner)
                        }

                        is UIResult.Error -> {
                            hide(spinner)
                            setUpErrorScreenBottomSheet(result.error)
                        }
                    }
                }
            }
        }
    }


    private fun initRecyclerViewWithSelection(schools: List<SchoolsItem>) {
        binding.recyclerView.also {
            it.layoutManager = LinearLayoutManager(requireContext())
            it.itemAnimator = DefaultItemAnimator()
            it.setHasFixedSize(true)
            val itemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            itemDecoration.setDrawable(
                ContextCompat.getDrawable(requireContext(), R.drawable.devider)!!
            )
            it.addItemDecoration(itemDecoration)
            val adapter = SchoolAdapter(requireContext(), schools)
            it.adapter = adapter
            adapter.onItemClick = { school ->
                val bundle = Bundle()
                bundle.putString(DetailsFragment.SCHOOL, Gson().toJson(school))
                findNavController().navigate(R.id.action_details_fragment, bundle)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}