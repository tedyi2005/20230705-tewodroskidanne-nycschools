package com.jpmc.android.fragments

import android.content.Context
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.jpmc.android.R
import com.jpmc.android.databinding.ErrorLayoutBinding
import com.jpmc.android.module.FragmentNavigationInterface
import com.jpmc.android.module.NavigationListener


/**
 * base class that has common reusable codes.
 */
abstract class BaseFragment : Fragment(), FragmentNavigationInterface {
    override var navigationListener: NavigationListener? = null
    private var onBackPressedCallback: OnBackPressedCallback? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        // This callback will only be called when MyFragment is at least Started.
        setOnBackPressedCallback { findNavController().popBackStack() }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (requireActivity() is NavigationListener) {
            setNavigationCallbackListener(requireActivity() as NavigationListener)
        }
    }

    override fun onDetach() {
        super.onDetach()

        navigationListener = null
    }

    override fun onDestroyView() {
        super.onDestroyView()

        onBackPressedCallback?.remove()
    }

    private fun setOnBackPressedCallback(lambda: () -> Unit) {
        onBackPressedCallback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            lambda()
        }
    }

    override fun setNavigationCallbackListener(listener: NavigationListener) {
        this.navigationListener = listener
    }

    /**
     * Sets up the toolbar from the current fragment
     *
     * @param   toolbar The toolbar to be set up
     */
    fun setUpToolbarFromFragment(toolbar: Toolbar) {
        navigationListener?.setUpToolbar(toolbar)
    }

    // Error bottom sheet
    fun setUpErrorScreenBottomSheet(
        message: String,
        buttonText: String = "",
        action: () -> Unit = {}
    ) {
        val errorLayoutBinding = ErrorLayoutBinding.inflate(layoutInflater)
        // custom style
        val bottomSheetDialog = BottomSheetDialog(
            ContextThemeWrapper(requireActivity(), R.style.BasicBottomSheetDialogTheme)
        )
        bottomSheetDialog.setContentView(errorLayoutBinding.root)
        // Canceling not allowed
        bottomSheetDialog.setCancelable(false)
        val standardBottomSheetBehavior =
            BottomSheetBehavior.from(errorLayoutBinding.root.parent as View)
        val bottomSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // Set Dragging off
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    standardBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        }
        standardBottomSheetBehavior.addBottomSheetCallback(bottomSheetCallback)
        bottomSheetDialog.show()

        // Apply click events
        errorLayoutBinding.apply {
            if (buttonText.isNotEmpty())
                retryButton.text = buttonText
            close.setOnClickListener {
                bottomSheetDialog.dismiss()
                if (buttonText.isNotEmpty())
                    action()
            }
            // set error text
            errorMessage.text = message
            retryButton.setOnClickListener {
                bottomSheetDialog.dismiss()
                action()
            }
        }
    }
}